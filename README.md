# Notes on Eloquent Javascript

I store all of my notes while reading Eloquent Javascript here. Feel free to read through my notes
and open issues if you want to open a dialogue.

## License

Notes are licensed under MIT.
