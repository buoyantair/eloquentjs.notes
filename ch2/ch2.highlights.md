# Program Structure

- "And my heart glows bright red under my filmy, translucent skin and they have to administer 10cc of JavaScript to get me to come back. (I respond well to toxins in the blood.) Man, that stuff will kick the peaches right out your gills!" By _why, Why's (Poignant) Guide to Ruby
- A fragment of code that produces a value is called an expression. 
- An expression between parentheses is also an expression, as is a binary operator applied to two expressions or a unary operator applied to one.
- Expressions can contain other expressions in a way similar to how subsentences in human languages are nested—a subsentence can contain its own subsentences, and so on. This allows us to build expressions that describe arbitrarily complex computations.
- If an expression corresponds to a sentence fragment, a JavaScript statement corresponds to a full sentence. A program is a list of statements.
- The simplest kind of statement is an expression with a semicolon after it.
```js
1;
false;
!true;
```
- In some cases, JavaScript allows you to omit the semicolon at the end of a statement. In other cases, it has to be there, or the next line will be treated as part of the same statement.
- You should imagine bindings as tentacles, rather than boxes. They do not contain values; they grasp them—two bindings can refer to the same value.
-  When you need to remember something, you grow a tentacle to hold on to it or you reattach one of your existing tentacles to it.
```js
let luigisDebt = 140;
luigisDebt = luigisDebt - 35;
console.log(luigisDebt);
// → 105
```
- When you define a binding without giving it a value, the tentacle has nothing to grasp, so it ends in thin air.

## The environment

- The collection of bindings and their values that exist at a given time is called the environment.

## Return values

- Anything that produces a value is an expression in JavaScript, 

## Control flow

- When your program contains more than one statement, the statements are executed as if they are a story, from top to bottom. 
- Not all programs are straight roads. We may, for example, want to create a branching road, where the program takes the proper branch based on the situation at hand. This is called conditional execution.

## Exercises

### Looping a Triangle

```js
// Iteration 1

for (let i = 0; i < 7; i++) {
  let sentence = '#';
  for (let j = 0; j < i; j++) {
    sentence += '#';
  }
  console.log(sentence);
}
```
This is probably the simplest exercise ever. Maybe I should try to make it more simply somehow... Right now it's running with a loop and then another loop inside it...

```js
// Iteration 2
const LEN = 7;
for (let i = 0; i < LEN; i++) {
  let sentence = '#'.repeat(i + 1);
  console.log(sentence);
}
```
This is looks much better now I dont have to worry about having a lot of loops in my code. But I still have a feeling that `String.repeat()`
might be repeating too! after all it has to append # to the text i + 1 times...

## FizzBuzz

```js
const START = 1;
const END = 200;

for (let i = START; i <= END; i++) {
  if (i % 3 === 0) {
    console.log('Fizz');
  } else if (i % 5 === 0) {
    console.log('Buzz');
  } else if (i % 3 === 0 && i % 5 === 0) {
    console.log('FizzBuzz');
  } else {
    console.log(i);
  }
}
```
This is what I got for my first try haha :D
Not so impressive but its fine.

## Chessboard

### First attempt
Looks like this works but It is very bad in terms of performance and how fast it runs?
because the complexity seems to be around n x n times what ever number is given...

```js
const size = 8
let board = ''
let toggle = false
for (let j = 0; j < size; j++) {
  let line = ''
  for (let i = 0; i < size; i++) {
    if (!toggle) {
      line += ' '
    } else if (toggle) {
      line += '#'
    }
    toggle = !toggle
  }
  toggle = !toggle
  board += `${line}\n`
}
console.log(board)
```

### Attempt 2

```js
const size = 8
let board = ''
let oddLine = ''
let evenLine = ''

for (let i = 0; i < size; i++) {
  const isEven = i % 2 === 0
  if (!isEven) {
    oddLine += '#'
    evenLine += ' '
  } else if (isEven) {
    oddLine += ' '
    evenLine += '#'
  }
}
const segment = `${oddLine}\n${evenLine}\n`
for (let i = 0; i < (size / 2); i++) {
  board += segment
}

console.log(board)
```
