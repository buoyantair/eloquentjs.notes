# Values, Types, And Operators

- "Below the surface of the machine, the program moves. Without effort, it expands and contracts. In great harmony, electrons scatter and regroup. The forms on the monitor are but ripples on the water. The essence stays invisibly below." by Master Yuan-Ma, The Book of Programming
- Inside the computer’s world, there is only data. You can read data, modify data, create new data—but that which isn’t data cannot be mentioned. All this data is stored as long sequences of bits and is thus fundamentally alike.
- Bits are any kind of two-valued things, usually described as zeros and ones. Inside the computer, they take forms such as a high or low electrical charge, a strong or weak signal, or a shiny or dull spot on the surface of a CD. Any piece of discrete information can be reduced to a sequence of zeros and ones and thus represented in bits.
- A typical modern computer has more than 30 billion bits in its volatile data storage (working memory).
- To be able to work with such quantities of bits without getting lost, we must separate them into chunks that represent pieces of information.

## Numbers
- JavaScript uses a fixed number of bits, 64 of them, to store a single number value. There are only so many patterns you can make with 64 bits, which means that the number of different numbers that can be represented is limited. 
- With N decimal digits, you can represent 10^n` numbers. Similarly, given 64 binary digits, you can represent 264 different numbers, which is about 18 quintillion (an 18 with 18 zeros after it). That’s a lot.
- Calculations with whole numbers (also called integers) smaller than the aforementioned 9 quadrillion are guaranteed to always be precise. 
- The actual maximum whole number that can be stored is more in the range of 9 quadrillion (15 zeros)—which is still pleasantly huge.
- Calculations with whole numbers (also called integers) smaller than the aforementioned 9 quadrillion are guaranteed to always be precise. 

## Boolean Values

### Comparision

- The way strings are ordered is roughly alphabetic but not really what you’d expect to see in a dictionary: uppercase letters are always “less” than lowercase ones, so "Z" < "a", and nonalphabetic characters (!, -, and so on) are also included in the ordering. When comparing strings, JavaScript goes over the characters from left to right, comparing the Unicode codes one by one.

### Logical Operators

- When mixing these Boolean operators with arithmetic and other operators, it is not always obvious when parentheses are needed. In practice, you can usually get by with knowing that of the operators we have seen so far, || has the lowest precedence, then comes &&, then the comparison operators (>, ==, and so on), and then the rest. This order has been chosen such that in typical expressions, as few parentheses as possible are necessary.

## Empty values

- The difference in meaning between undefined and null is an accident of JavaScript’s design, and it doesn’t matter most of the time.

## Automatic type conversion

- When you keep getting NaN as a response to an operation, try to look out for accidental type conversion.
- when null or undefined occurs on either side of the boolean operator, it produces true only if both sides are one of null or undefined.
- I recommend using the three-character comparison operators defensively to prevent unexpected type conversions from tripping you up.
