# Chapter Overview

## Sections

### Introduction

#### Questions & Answers

1. What is impossible for a computer to do?

2. Can programming computers be done with anything other than programming languages?

#### Comparision

#### Section Overview

Programming is all about giving instructions to computers, which are often dumb machines. Programming
a computer involves making use of a programming language to communicate the steps to be performed
by the computer. A programming language is the human-computer interface that enables us to interact
with the computer in a much more precise manner. Programming gives us the freedom to freely
communicate with the machines to help make our lives easier thanks to the superior computational ability of a computer and via automation of boring tasks. Programming enables us to think about ideas
in more abstract and clear ways. Javascript is a programming language shipped with almost every modern web browser.

#### Further resources / references

#### Quotes

"We think we are creating the system for our own purposes. We believe we are making it in our own image... But the computer is not really like us. It is a projection of a very slim part of ourselves: that portion devoted to logic, order, rule, and clarity."

- Ellen Ullman, Close to the Machine: Technophilia and its Discontents

### On Programming

#### Questions & Answers

1. How to control the complexity while building computer programs?

2. Is reading code an effective way to learn programming?

3. How does programming help in my day to day life?

4. What kind of discipline (and state of mind) does programming require?

5. How does mathematics and logic play in the domain of programming?

#### Comparision

#### Section Overview

Programming is difficult. Though we use programming because computers are very fast at the things they
do. Often times we end up writing programs and then getting lost in our own creation. To learn
programming is to make mistakes and to learn from those mistakes. Programming cannot be learnt
from a set of "best practices" curated by even the best of the programmers. Programming is like an art
form and must be learnt by practice. Computer programs become more and more complex as you keep
adding new features and functionality to and around them. Without care a program will grow out of
control, confusing even the person who created it. The art of programming is the skill of
controlling complexity. The art of programming is to design simple programs and this is one of the
biggest challenges of programming. You are building your own maze, in a way, and you might just get
lost in it. Learning is hard work, but everything you learn is yours and will make subsequent
learning easier.

#### Further resources / references

#### Quotes

"When action grows unprofitable, gather information; when information grows unprofitable, sleep."

- Ursula K. Le Guin, The Left Hand of Darkness

### Why language matters

- To program early computers, it was necessary to set large arrays of switches in the right position or punch holes in strips of cardboard and feed them to the computer.
- the same program can be expressed in both long and short, unreadable and readable ways.
- A good programming language helps the programmer by allowing them to talk about the actions that the computer has to perform on a higher level.

### What is Javascript?

- JavaScript was introduced in 1995 as a way to add programs to web pages in the Netscape Navigator browser.
- After its adoption outside of Netscape, a standard document was written to describe the way the JavaScript language should work so that the various pieces of software that claimed to support JavaScript were actually talking about the same language. This is called the ECMAScript standard, after the Ecma International organization that did the standardization. In practice, the terms ECMAScript and JavaScript can be used interchangeably—they are two names for the same language.
-  ECMAScript version 3 was the widely supported version in the time of JavaScript’s ascent to dominance, roughly between 2000 and 2010. During this time, work was underway on an ambitious version 4, which planned a number of radical improvements and extensions to the language. Changing a living, widely used language in such a radical way turned out to be politically difficult, and work on the version 4 was abandoned in 2008, leading to a much less ambitious version 5, which made only some uncontroversial improvements, coming out in 2009. Then in 2015 version 6 came out, a major update that included some of the ideas planned for version 4. Since then we’ve had new, small updates every year.

### Code & What to do with it

- I believe reading code and writing code are indispensable parts of learning to program.

